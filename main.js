var express    = require("express");
var session = require('express-session');
var file = require('./routes/file');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var expressValidator = require('express-validator');

var app = express();
app.use(session({secret: 'ssshhhhh'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(bodyParser.json());
var sess;
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var router = express.Router();


// function requireLogin(req, res, next) {
//   if (req.session.isLogin) {
//     next(); // allow the next route to run
//   } else {
//     res.send({"code":412,"eroor":"please Login frist"});
//   }
// }

// test route
router.get('/', function(req, res) {
    res.json({ message: 'Successfully Runing....' });
});

//route to handle user registration
router.post('/register',file.register);
router.post('/login',file.login);
router.post('/forgetpassword',file.forgetpassword);
router.post('/getuser',file.getuser);
router.post('/update_user',file.update_user);
router.post('/delete_user',file.delete_user);
router.post('/chnage_status',file.chnage_status);
router.post('/chnage_password',file.change_password);
app.use('/api', router);
app.listen(3000);