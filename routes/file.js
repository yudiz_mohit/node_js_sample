var bcrypt = require('bcrypt');

var connection = require('./../config');


var nodemailer = require("nodemailer");
var smtpTransport = nodemailer.createTransport({
    service: "gmail",
    host: "smtp.gmail.com",
    auth: {
        user: "mohit.bhesaniya@ping2world.com",
        pass: "8347394010"
    }
});



exports.register = function(req,res){
  var today = new Date();


  req.checkBody('first_name', 'First Name is Required').notEmpty();
  req.checkBody('last_name', 'Last Name is Required').notEmpty();
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('password', 'Password is Required').notEmpty();
  req.checkBody('password', 'Password len 5 to 20 required').len(5,20);

   var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }else{

        bcrypt.hash(req.body.password, 5, function( err, bcryptedPassword) {
         var users={
           "vEmail":req.body.email,
           "vPassword":bcryptedPassword,
           "vFirstName":req.body.first_name,
           "vLastName":req.body.last_name,
           "dCreated":today,
         }
            var email= req.body.email;
            connection.query('SELECT * FROM tbl_user WHERE vEmail = ?',[email], function (error, results, fields) {
            if (results.length == 0) { 
                connection.query('INSERT INTO tbl_user SET ?',users, function (error, results, fields) {
                if (error) {
                  res.send({ "code":400, "failed":"error ocurred"})
                }else{
                  res.send({ "code":200, "success":"User registered successfully."});
                }
                });
            }else{
              res.send({"code":412,"success":"Email Exit;"});
            }    
          });    
        })
    }      
};



exports.login = function(req,res){
  var email= req.body.email;
  var password = req.body.password;
  
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('password', 'Password is Required').notEmpty();
  
   var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }else{
        connection.query('SELECT iUserId FROM tbl_user WHERE vEmail = ?',[email], function (error, results, fields) {
        if (error) {
          res.send({"code":400,"failed":"error ocurred"})
        }else{
          if(results.length > 0){
               // sess = req.session;
               // sess.isLogin= true;
              res.send({"code":200,"success":"successfully login....","data" :results});
          }else{
           
            res.send({"code":204,"success":"Email does not exits"});
          }
        }
        });
    }    
}


exports.forgetpassword = function(req,res){

  var email = req.body.email;
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  
  var errors = req.validationErrors();

  if (errors) {
      res.send({"code":400,"failed":errors});
  }else{
      var random = require("random-js")();
       var vPassNew = random.integer(100000, 999999);
       bcrypt.hash(vPassNew, 5, function( err, bcryptedPassword) {
         var email= req.body.email;
          connection.query('UPDATE tbl_user SET vPassword = ? WHERE vEmail = ?',[bcryptedPassword,email], function (error, results, fields) {
            if (error) {
              console.log("error ocurred",error);
              res.send({"code":400,"failed":"error ocurred"})
            }else{
                var mailOptions={
                    from: 'mohit.bhesaniya@ping2world.com', // sender address
                    to: email, // list of receivers
                    subject: 'ForgetPassword Node Js', // Subject line
                    html: 'Here is your old password <b>' + vPassNew + '</b>' // html body
                }
                console.log(mailOptions);
                smtpTransport.sendMail(mailOptions, function(error, response){
                  if(error){
                      res.end("error PLease check your mail smtp");
                  }else{
                    console.log("Message sent: " + response.message);
                    res.end("sent");
                  }
                });
            }
          });    
        });    
    }
};


exports.change_password = function(req,res){
  req.checkBody('userid', 'User id is Required').notEmpty();
  req.checkBody('old_password', 'Password is Required').notEmpty();
  req.checkBody('old_password', 'Password len 5 to 20 required').len(5,20);
  req.checkBody('new_password', 'New password is Required').notEmpty();
  req.checkBody('new_password', 'New password len 5 to 20 required').len(5,20);
  req.checkBody('confirm_new_password', 'Confirm Password is Required').notEmpty();
  req.checkBody('confirm_new_password', 'Confirl Password len 5 to 20 required').len(5,20);
  
  var errors = req.validationErrors();

  if (errors) {
      res.send({ "code":400, "failed":errors });
  }else{

      var email = req.body.email;
      var userid = req.body.userid;
      var opass = req.body.old_password;
      var npass = req.body.new_password;
      var cpass = req.body.confirm_new_password;   
      
      if (npass == cpass){
             connection.query('SELECT * FROM tbl_user WHERE vEmail = ?AND iUserID = ?',[email,userid], function (error, results, fields) {
              if (error) {
                res.send({ "code":400, "failed":"error ocurred" })
              }else{
                  if(results.length > 0){ 
                        bcrypt.compare(opass, results[0].vPassword, function(err, doesMatch){
            
                          if (doesMatch){
                               bcrypt.hash(npass, 5, function( err, bcryptedPassword) {
                                  connection.query('UPDATE tbl_user SET vPassword = ? WHERE  iUserID = ?',[bcryptedPassword,userid], function (error, results, fields) {
                                    if (error) {
                                          res.send({ "code":400,"error":"Invalid Authentication" })
                                    }else{
                                          res.send({ "code":200,"success": "successfully chnage password." })
                                    }
                                  });    
                              });
                          }else{
                            res.send({"code":412,"error":"Old password wrong"});
                          }
                      });  
                  }else{
                      res.send({ "code":412, "error":"Athentication failed."});
                  }
               }      
             }); 
        }else{
          res.send({ "code":204, "error":"Confirm password not match."});
        }
    }
};

exports.getuser = function(req,res){
    connection.query('SELECT * FROM tbl_user WHERE status != "d"',function (error, results, fields) {
      if (error) {
        res.send({
          "code":400,
          "failed":"error ocurred"
        })
      }else{
           if(results.length > 0){
              res.send({"code":200,"success":"success.","data" :results});
           }else{
             res.send({"code":400,"failed":"User Data Not Found."})
           }     
      }
    });    
      
};

exports.update_user = function(req,res){
  var today = new Date();

  req.checkBody('first_name', 'First Name is Required').notEmpty();
  req.checkBody('last_name', 'Last Name is Required').notEmpty();
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('userid', 'User id is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  
   var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }else{
      var userid  =req.body.userid;
      var first_name = req.body.first_name;
      var last_name = req.body.last_name;
      var email= req.body.email;

      connection.query('SELECT * FROM tbl_user WHERE vEmail = ? AND iUserID  != ?',[email,userid], function (error, results, fields) {
        if (results.length == 0) { 
               var sql = "UPDATE tbl_user SET vEmail = '"+email+"',vFirstName = '"+first_name+"',vLastName = '"+last_name+"' WHERE iUserID ="+userid;
              connection.query(sql, function (err, result) {
                if (err) {
                  console.log(err);
                  res.send({"code":400,"failed":"error ocurred"})
                }else{
                  res.send({"code":200,"success":result.affectedRows + " record(s) updated",});
                }   
              });
        }else{
          res.send({"code":412,"success":"Email Exit"});
        }    
      });    
    }      
};


exports.delete_user = function(req,res){
  var today = new Date();

  req.checkBody('userid', 'User id is Required').notEmpty();
  
   var errors = req.validationErrors();

  if (errors) {
      res.send({"code":400,"failed":errors});
  }else{
      var userid  =req.body.userid;

      connection.query('SELECT * FROM tbl_user WHERE  iUserID = ?',[userid], function (error, results, fields) {
        if (results.length > 0) { 
               var sql = "UPDATE tbl_user SET status = 'd'  WHERE iUserID ="+userid;
              connection.query(sql, function (err, result) {
                if (err) {
                  console.log(err);
                  res.send({"code":400,"failed":"error ocurred"})
                }else{
                  res.send({"code":200,"success":"records deleted: " + result.affectedRows});
                }   
              });
        }else{
          res.send({"code":412,"success":"User Not Exit"});
        }    
      });    
    }      
};

exports.chnage_status = function(req,res){
  var today = new Date();

  req.checkBody('userid', 'User id is Required').notEmpty();
  req.checkBody('status', 'Status is Required').notEmpty();
  
   var errors = req.validationErrors();

  if (errors) {
      res.send({"code":400,"failed":errors});
  }else{
      var userid  =req.body.userid;
      var status  =req.body.status;

      connection.query('SELECT * FROM tbl_user WHERE  iUserID = ? AND status != "d"' ,[userid], function (error, results, fields) {
        if (results.length > 0) { 
               var sql = "UPDATE tbl_user SET status = '"+status+"'  WHERE iUserID ="+userid;
              connection.query(sql, function (err, result) {
                if (err) {
                  console.log(err);
                  res.send({"code":400,"failed":"error ocurred"})
                }else{
                  res.send({"code":200,"success":"successfully status change"});
                }   
              });
        }else{
          res.send({"code":412,"success":"User Not Exit"});
        }    
      });    
    }      
};

